<?php
// cat query1.pdb | cut -c30-53 | sed -e 's/\([ ]\) */\1/g' -e 's/^\ //' | tr ' ' '\n' > query1.pdb.part-trans

if(isset($_GET["from"])){
  $search_dir = $_GET["from"];
}else{
  $search_dir = "/srv/home/www/html/super/searches/";
}

if(isset($_GET["pdbid"])){
  $pdbid = strtoupper($_GET["pdbid"]);
}else{
  $pdbid = "1hho";
}

if(isset($_GET["chain"])){
  $chain = $_GET["chain"];
}else{
  $chain = "A";
}

if(isset($_GET["start"])){
  $start = $_GET["start"];
}else{
  $start = "VAL5";
}

if(isset($_GET["end"])){
  $end = $_GET["end"];
}else{
  $end = "VAL32";
}

/* Fix for ProDy needing a HOME directory */
putenv("HOME=/tmp/");

/*$get = "http://www.pdb.org/pdb/files/" . $pdbid . ".pdb";
$dest = $search_dir . "/pdb" . $pdbid . ".ent";
$command = "wget -nc --quiet -O " . $dest . " " . $get;
shell_exec($command);*/
$directory = strtolower(substr($pdbid, 1, 2));
$dest = "/srv/data01/pdb/" . $directory . "/pdb" . strtolower($pdbid) . ".ent.gz";

$gap = intval(shell_exec("cat " . $search_dir . "/gap_data"));
$stump = intval(shell_exec("cat " . $search_dir . "/stump_data"))+1;

//Query structure
$command = "python get_coords.py " . $search_dir . "/query1.pdb > " . $search_dir . "/query.txt";
shell_exec($command);

//Reference structure
$command = "python get_coords.py -r --from=" . $start . " --to=" . $end . " --chain=" . $chain . " " . $dest . " > " . $search_dir . "/ref.txt";
shell_exec($command);

if($gap != "0"){
  shell_exec("sed -e '" . $stump . "," . ($stump + $gap - 1) . "d' " . $search_dir . "/ref.txt > " . $search_dir . "/ref.tmp");
  shell_exec("mv " . $search_dir . "/ref.tmp " . $search_dir . "/ref.txt");
}

//Transform
$command = "./transformer " . $search_dir . "/query1.pdb " . $search_dir . "/query.txt " . $search_dir . "/ref.txt > " . $search_dir . "/qry-t.pdb";
shell_exec($command);

$retfilename = $search_dir . "/transformed-" . strval(rand()) . ".pdb";
$command = "ln -s " . $search_dir . "/qry-t.pdb " . $retfilename;
shell_exec($command);

//Link to the PDB file for a Jmol visualisation which is expecting the file to be under $_SERVER["DOCUMENT_ROOT"]
$command = "ln -s " . $dest . " " . $search_dir . strtoupper($pdbid) . ".gz";
shell_exec($command);

echo substr($search_dir . $pdbid . ".gz", 25) . "|" . substr($retfilename, 25);
?>