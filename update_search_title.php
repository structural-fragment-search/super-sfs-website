<?php
/* Get session data
 * Change the title in results.txt
 * Add new link to saved_searches.html
 */

error_reporting(E_ALL | E_STRICT);

/* START Setup Configuration */
require_once( "superSetup.php" );
/* END Setup Configuration */

$sessionID = $_POST["session"];
$search_count = $_POST["count"];
$newtitle = $_POST["newtitle"];

$saved = "$super_path/searches/$sessionID/saved_searches.html";
$results = "$super_path/searches/$sessionID/search$search_count/results.txt";

//Firstly, results title...
shell_exec("cat " . $results . " | sed s/Title:\ .*/Title:\ " . escapeshellarg($newtitle) . "/ > " . $results . ".tmp");
shell_exec("mv " . $results . ".tmp " . $results);

//Then saved results
$search_result = trim(shell_exec("grep \"id=" . $search_count . "\" " . $saved)); //saved line
$line_number = trim(shell_exec("grep -n \"id=" . $search_count . "\" " . $saved . " | cut -d \":\" -f 1")); //line number
shell_exec("sed '" . $line_number . "d' " . $saved . " > " . $saved . ".tmp");
shell_exec("echo " . escapeshellarg($search_result) . " | sed s/'title=.*\">'/'title=" . rawurlencode($newtitle) . "\">'/ | sed s/'>.*@'/'>" . $newtitle ."\ @'/ >> " . $saved . ".saved");
shell_exec("cat " . $saved . ".tmp " . $saved . ".saved > " . $saved);
shell_exec("rm -f " . $saved . ".tmp " . $saved . ".saved");
?>