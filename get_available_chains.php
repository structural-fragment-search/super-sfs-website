<?php
error_reporting(E_ALL | E_STRICT);

/* START Setup Configuration */
require_once( "superSetup.php" );
/* END Setup Configuration */

if(isset($_GET["pdbid"])){
  $pdbid = strtolower($_GET["pdbid"]);
}else{
  error_log("pdbid is not defined.");
}

clearstatcache();
$directory = substr($pdbid, 1, 2);
$dest = "$pdb_path/$directory/pdb${pdbid}.ent.gz";
$filestat = stat($dest);
if($filestat == FALSE){
  echo "!";
  return;
}else{
  $command = "$Python_Binary get_coords.py --list-chains " . $dest;
  echo shell_exec($command);
}
?>