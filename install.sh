#!/bin/bash

PREFIX=$1

if [ "0${PREFIX}" == "0" ]; then
    echo "I need to know where to install Super-Web"
    exit
fi

echo "Installing Super-Web to $PREFIX"

echo -n "Creating directories... "
if [ ! -d "${PREFIX}/scripts" ]; then
    mkdir -p "${PREFIX}/scripts" "${PREFIX}/scripts/_firebug";
    echo "scripts/";
fi

if [ ! -d "${PREFIX}/styles" ]; then
    mkdir -p "${PREFIX}/styles";
    echo "styles/";
fi

if [ ! -d "${PREFIX}/images" ]; then
    mkdir -p "${PREFIX}/images";
    echo "images/";
fi

if [ ! -d "${PREFIX}/jmol" ]; then
    mkdir -p "${PREFIX}/jmol";
    echo "jmol/";
fi

if [ ! -d "${PREFIX}/examples" ]; then
    mkdir -p "${PREFIX}/examples";
    echo "examples/";
fi

if [ ! -d "${PREFIX}/searches" ]; then
    mkdir -p "${PREFIX}/searches";
    echo "searches/";
fi
echo "[DONE]"

echo -n "Installing scripts... "
for script in scripts/*; do
    cp -r $script "${PREFIX}/scripts/";
done
echo "[DONE]"

echo -n "Installing styles... "
for style in styles/*; do
    cp $style "${PREFIX}/styles/";
done;
echo "[DONE]"

echo -n "Installing images... "
for image in images/*; do
    cp $image "${PREFIX}/images/";
done;
echo "[DONE]"

echo -n "Installing code... "
for html in *.html; do
    cp $html "${PREFIX}/";
done
for php in *.php; do
    cp $php "${PREFIX}/";
done
for pyth in *.py; do
    cp $pyth "${PREFIX}/";
done
echo "[DONE]"

echo "Finished installing Super-Web to $PREFIX"