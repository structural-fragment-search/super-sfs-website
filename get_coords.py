#!/usr/bin/python

import prody
from optparse import OptionParser

def uniq(arr):
    uList = []
    for i in arr:
        if not i in uList:
            uList.append(i)
    return uList

def argerr(message=None):
    if(message):
        print message
    opts.print_help()
    exit()

prody.setVerbosity('none')

usage = "usage: %prog [options] PDBFILE [OUTPUTFILE]"

opts = OptionParser(usage=usage)
opts.add_option("-o", "--output", dest="output", default="queryrawdata")
opts.add_option("-c", "--chain", dest="chain", default="all")
opts.add_option("-f", "--from", action="store", type="int", dest="start", default=1)
opts.add_option("-t", "--to", action="store", type="int", dest="end", default=0)
opts.add_option("-l", "--list-chains", action="store_true", dest="listmode", default=False)
opts.add_option("-g", "--generate-query", action="store_true", dest="generate", default=False)
opts.add_option("-r", "--generate-reference", action="store_true", dest="ref", default=False) #do the right thing with specified reference sets

(options, args) = opts.parse_args()

#1: PDB file; 2:output file
if len(args) == 0:
    argerr("No files to work on.")

try:
    inprot = prody.parsePDB(args[0])
except IOError, e:
    if e[0] == 2:
        argerr("File not found: " + args[0])
    else:
        print e
        exit()
index = 1

if(options.listmode):
    if len(args) != 1:
        argerr("You need to supply a PDB file name only.")
    ul = uniq(inprot.calpha.getChids())
    for u in ul:
        print u
    quit()
elif(options.generate):
    if len(args) != 2:
        argerr("You need to supply input and output filenames.")
    #selstring = "(calpha) and (chain " + str(options.chain) + ") and (resnum " + str(options.start) + " to " + str(options.end) + ")"
    #sel = inprot.select(selstring)
    sel = inprot[str(options.chain)].calpha.select("(resnum " + str(options.start) + " to " + str(options.end) + ")")
    try:
        prody.writePDB(args[1], sel)
    except TypeError, t:
        print t
    quit()

# BUG: Need to search for insertion code as well
elif(options.ref):
    if len(args) != 1:
        argerr("You need to supply one input PDB file.")

    selstring = "(calpha) and (chain " + str(options.chain) + ") and (resnum " + str(options.start) + " to " + str(options.end) + ")"
    sel = inprot.select(selstring)
    for atom in sel:
        c = atom.getCoords()
        print '%.3f %.3f %.3f' % (c[0], c[1], c[2])

else:
    if len(args) != 1:
        argerr("You need to supply one input PDB file.")

    for atom in inprot.calpha:
        c = atom.getCoords()
        print '%.3f %.3f %.3f' % (c[0], c[1], c[2])
