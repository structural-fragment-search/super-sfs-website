#!/bin/bash

DIR=$1;
COUNTER=1;

if [ -f $DIR/amalgamated ]; then
    exit;
else
    touch $DIR/amalgamated;
fi

for resfile in $DIR/results.[0-9]; do
    echo "#Query ${COUNTER}" >> $DIR/results.txt;
    cat $resfile >> $DIR/results.txt;
    let COUNTER=$COUNTER+1;
done