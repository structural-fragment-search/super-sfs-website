<?php
$sessionID = $_GET["userid"];
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Super: Previous Searches</title>
    <link rel="stylesheet" type="text/css" href="/styles/super.css" />
<?php readfile("../scripts/analytics.js"); ?>
  </head>
  <body>
    <h1 style="text-align:center;">Super: Previous Searches</h1>
    <hr style="width:40%;"/>
    <p><?php include("searches/" . $sessionID . "/saved_searches.html"); ?></p>
  </body>
</html>
