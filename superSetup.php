<?php
/* Some environment variables */
putenv("HOME=/tmp/"); // Fix for ProDy needing a HOME directory
putenv("PYTHONPATH=/home/jhcol1/.local/lib/python2.7/site-packages"); //Server location of prody package

/* Some configuration variables */
$web_root = "/~jhcol1";
$super_web_path = "$web_root/super";

$root_path = "/home/jhcol1/public_html";

$pdb_path = "/home/jhcol1/pdb/";

$super_path = "$root_path/super";
$super_plugin_path = "$super_path/lib/";


/* Programs needed */
$Python_Binary = "/opt/python/bin/python2.7";

/* Super Configuration */
$CPUCores = 4;
?>