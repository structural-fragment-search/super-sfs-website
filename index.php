<?php
error_reporting(E_ALL | E_STRICT);

/* START Setup Configuration */
require_once( "superSetup.php" );
/* END Setup Configuration */

//Initialise a new session or re-establish existing session
session_name("_SuperUserID");
session_set_cookie_params(30 * 24 * 60 * 60, $super_web_path);
session_start();

if(isset($_SESSION)){
  if(isset($_SESSION["search_count"])){
    $search_count = & $_SESSION["search_count"];
  } else {
    $_SESSION["search_count"] = 0;
    $search_count = & $_SESSION["search_count"];
  }
} else {
  $search_count = -1;
}

$sessionID = session_id();
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>Super: Structural Fragment Search</title>
  <link rel="stylesheet" type="text/css" href="styles/super.css" />
  <meta name="description" content="Rapidly screens protein structure databases to identify all fragments that superpose with a query uder a prespecified threshold of RMSD.">
  <meta name="keywords" content="fragment search, PDB search, PDB screening, PDB scan, superpose, fit, 3D, superposable fragments">
  <meta name="author" content="James Collier, Arthur Lesk, Maria Garcia de la Banda, Arun Konagurthu">

  <script type="text/javascript" src="scripts/dojo.js"></script>
  <script type="text/javascript" src="scripts/behaviour.js"></script>
  <script type="text/javascript">
    var copied_fragments = Array();
    var target_fragments = Array();
    var search_database = "pdb.db";

    dojo.ready(function(){
        if(dojo.isIE <= 8){
	    var warn = dojo.byId("warnings");
	    warn.innerHTML = warn.innerHTML + "<br /> Apologies, this site has not received much testing with Internet Explorer (and is known not to work with versions <=8, we recommend the use of an alternative browser such as Mozilla Firefox, Safari, Opera, Epiphany or Google Chrome in the mean-time.";
	}
	if(dojo.isIE == 9){
	    var warn = dojo.byId("warnings");
	    warn.innerHTML = warn.innerHTML + "<br /> Apologies, this site has not received testing with Internet Explorer 9, we recommend the use of an alternative browser such as Mozilla Firefox, Safari, Opera, Epiphany or Google Chrome in the mean-time.";
	}
	copied_fragments.push(dojo.byId("fragment_input1"));
	var div = dojo.create("div", {id:"pdb_id_input0"}, "specify_container");
	var form0 = new specifyObject(-1, div);
	target_fragments.push(form0);
	    <?php if(isset($_GET["example"])){readfile("examples/load_example_query.js");} ?>
    });

function load_example(){
    var appnd = "<?php if(array_key_exists("example", $_GET)){echo "";} else { echo "?example"; } ?>";
    window.location.href=window.location.href+appnd;
}

function clear_example() {
    window.location = window.location.toString().split('?')[0]
}

function update_database(db){
    switch(db){
    case 1:
	search_database = "pdb.db";
	break;
    case 2:
	search_database = "nonredundant";
	break;
    default:
	search_database = "pdb.db";
	break;
    }

    dojo.byId("db1").value = search_database;
    dojo.byId("db2").value = search_database;
    dojo.byId("db3").value = search_database;
}
  </script>
<?php readfile("scripts/analytics.js"); ?>
</head>
<body>
  <noscript>
    <div id="warnings" style="color:red; text-align:center; border:5px solid red; padding:5px; background-color:yellow;">
      Note that this site requires client side processing using javascript. Please enable javascript in your web-browser and/or allow javascript to run on this page.
    </div>
  </noscript>
  <h1 style="text-align:center; width:85%; margin:auto;">Super: A web server to rapidly screen superposable oligopeptide fragments from the Protein Data Bank</h1>
  <p id="citation"><span>Citation:</span> <a href="http://lcb.infotech.monash.edu.au/wiki/index.php/User:James">James H. Collier</a>, <a href="http://bmb.psu.edu/directory/aml25">Arthur M. Lesk</a>, <a href="http://www.csse.monash.edu.au/~mbanda/">Maria Garcia de la Banda</a> and <a href="http://www.csse.monash.edu.au/~karun/Site/Home.html">Arun S. Konagurthu</a>, Super: A web server to rapidly screen superposable oligopeptide fragments from the Protein Data Bank. <i>Nucleic Acids Research</i> (in press; to appear in July 2012 Web Server Issue)</p>
  <hr style="width:40%"/>
  <span style="margin:auto; text-align:center; display:block;"><a href="help/">Click here for online help documentation.</a></span>
  <p style="text-align:center; padding:0.5em;">
  Please choose <u>one</u> of the following methods for specifying queries.<?php if($search_count > 0){echo ' Or, <a href="previous_searches.php?userid=' . htmlspecialchars($sessionID) . '" target="_blank">view previous searches</a>.';} ?> <?php if(!array_key_exists("example", $_GET)){ echo "Or, <input type=\"button\" value=\"Load test data\" onclick=\"load_example()\" />"; } else { echo "Or, <input type=\"button\" value=\"Clear test data\" onclick=\"clear_example()\" />"; } ?>
  </p>
  <div class="input-wrapper" style="text-align:center; background-color:#E8E8E8; margin:5px auto; padding:5px; width:85%">
    Select database to search:&nbsp;
    <input type="radio" name="database" value="pdb.db" onClick="update_database(1)" checked>PDB</input>
    <input type="radio" name="database" value="nonredundant" onClick="update_database(2)">Non-redundant PDB</input>
  </div>

  <!--
      This DIV is a wrapper for the 3 input types. It ensures child DIVs can be horizontally aligned and vertically aligned.
    -->
  <div id="submission" class="input_wrapper" style="background-color:#E8E8E8; padding:5px;">
    <!--<div style="background-color:blue; margin:5px; padding:5px;">-->
      <h3>Search PDB using a contiguous fragment.</h3>
    <!--
	This DIV contains a form for copying and pasting fragments
      -->
    <div id="pdb_frag" class="data_area">
      <div class="data_header">
	<h3>Paste PDB Fragment as a Query</h3>
      </div>
      <form name="PDB_paste_query" method="post" action="search_results.php">
	<input type="hidden" id="paste_count" name="number" value="1"/>
	<input type="hidden" name="method" value="paste"/>
	<input type="hidden" name="sessionID" value="<?php echo $sessionID; ?>"/>
	<input id="db1" type="hidden" name="database" value="pdb.db">
	<p style="font-size:0.8em;">
	  Copy and paste a query PDB fragment into the text box below.<!-- If you wish to add more queries, click on the '+' button below.-->
	</p>
	<p style="font-size:0.8em; padding:0;">
	Threshold: <input type="number" name="threshold" style="margin:0;" size="4" min="0.0" max="3.5" step="0.1" value="2.0"/> Angstroms (RMSD)</p><hr/>
	<div id="error1" style="background-color:yellow; float:right; visibility:hidden;">Empty!</div>
	<textarea name="input1" id="fragment_input1" class="fragment_input" rows="15" cols="80" wrap="off" onclick="clearErrors(1)"><?php if(isset($_GET["example"])){readfile("examples/pdb_fragment.pdb");} ?></textarea><br/>
<!--	<input type="button" id="add_frag" class="add_fragments" onclick="addFragment(this.id)" disabled="disabled" title="Disabled"/>--><br/>
	Display <input type="text" name="displayNumber" size="3" value="70" /> results.
	<input type="button" id="search_frag" value="Search" onclick="submitForm(this.form)"/>
      </form>
    </div>

    <!--
	This DIV contains a form for specifying fragments with an ID and start and end amino acids
      -->
    <div id="pdb_id" class="data_area">
      <div class="data_header">
	<h3>Specify PDB Range as a Query</h3>
      </div>
      <form name="PDB_specify_query" method="post" action="search_results.php">
	<input type="hidden" id="specify_count" name="number" value="1"/>
	<input type="hidden" name="method" value="specify"/>
	<input type="hidden" name="sessionID" value="<?php echo $sessionID; ?>"/>
	<input id="db2" type="hidden" name="database" value="pdb.db">
	<p style="font-size:0.8em;">
	  Enter the position of a fragment in the PDB. <!--If you wish to add more queries, click on the '+' button below.--> <span style="color:red;">There may be a slight delay validating the PDBID and loading chain identifiers.</span><br/>
	</p>
	<p style="font-size:0.8em; padding:0;">
	  Threshold: <input type="number" name="threshold" style="margin:0;" size="4" min="0.0" max="3.5" step="0.1" value="2.0"/> Angstroms (RMSD)</p><hr/>
	  <label for="pdb_id_id0" style="margin-right:2em;">PDB ID:</label>
	  <label for="pdb_id_id0">Chain:</label>
	  <label for="pdb_id_id0" style="margin-right:2em;">Start:</label>
	  <label for="pdb_id_id0">End:</label><br/>
	  <span style="display:inline-block;" id="specify_container" onclick="clearErrors(2);">

	  </span> <a id="help_res_range" href="javascript:alert('Start and end amino-acids may be entered as residue numbers (e.g. 123) or as three-letter-codes followed by residue numbers (e.g. VAL123).');">?</a>
	  <div id="error2" style="background-color:yellow; float:right; visibility:hidden;">Empty!</div>
	  <div id="warn2" style="background-color:red; float:right; visibility:hidden;">Invalid!</div>
<!--	<input type="button" id="add_id" class="add_fragments" onclick="addFragment(this.id)" disabled="disabled" title="Disabled"/>--><br/>
	Display <input type="text" name="displayNumber" size="3" value="70" /> results.
	<input type="button" id="search_specify" value="Search" onclick="submitForm(this.form)"/>
	<br/>
	<div style="clear:both;"></div>
      </form>
    </div>
    
    <!--
	This DIV contains a form allowing the upload of an archive of fragments.
      -->
    <!--
    <div id="pdb_file" class="data_area">
      <div class="data_header">
	<h3>Upload .tar File Containing PDB Formatted Queries</h3>
      </div>
      <p style="font-size:0.8em;">
	Upload a .tar achive of PDB format query fragments. <span style="color:red;">This option is not yet implemented.</span><br/>
      </p>
      <form name="PDB_upload_query" method="post" action="search_results.php" enctype="multipart/form-data">
	<input type="hidden" name="number" value="0"/>
	<input type="hidden" name="method" value="upload"/>
	<input type="hidden" name="sessionID" value="<?php echo $sessionID; ?>"/>
	<p style="font-size:0.8em; padding:0;">
	  Threshold: <input type="number" name="threshold" style="margin:0;" size="4" min="0.0" max="3.5" step="0.1" value="2.0"/> Angstroms (RMSD)</p><hr/>
	<input type="file" name="fragments" accept="application/x-tar"/><br/>
	Display <input type="text" name="displayNumber" size="3" value="70" /> results.
	<input type="button" id="search_frag" value="Search" onclick="submitForm(this.form)" disabled="disabled"/>
      </form>
    </div>
    -->
    <!--
    <div style="clear:both;">
    </div>
    <br/>-->
    <br style="margin:10px; clear:both;"/>
  </div>
  <hr style="width:80%;"/>
  <div class="input_wrapper" style="background-color:#E8E8E8; padding:5px; margin-top:5px;">
      <h3>Search PDB using two fragments with a prespecified gap between them.</h3>
    <div id="pdb_fuzzy" class="data_area">
      <div class="data_header">
	<h3>Paste gapped query fragment</h3>
      </div>
      <form name="PDB_stump_query" method="post" action="search_results.php">
	<input type="hidden" name="number" value="2"/>
	<input type="hidden" name="method" value="stumps"/>
	<input type="hidden" name="sessionID" value="<?php echo $sessionID; ?>"/>
	<input id="db3" type="hidden" name="database" value="pdb.db">
	<p style="font-size:0.8em; padding:0;">
	  Threshold: <input type="number" name="threshold" style="margin:0;" size="4" min="0.0" max="3.5" step="0.1" value="2.0"/> Angstroms (RMSD)</p><hr/>
	<div style="float:left; width:40%;">
	  <p style="font-size:0.8em; margin:0; padding:0;">
	    Paste the first portion of the fragment here.
	  </p><br/>
	  <textarea name="stump1" id="stump_input1" class="fragment_input" style="width:100%;" rows="15" wrap="off"><?php if(isset($_GET["example"])){readfile("examples/pdb_stump1.pdb");} ?></textarea>
	</div>
	<div style="float:right; width:40%;">
	  <p style="font-size:0.8em; margin:0; padding:0;">
	    Paste the last portion of the fragment here.
	  </p><br/>
	  <textarea name="stump2" id="stump_input2" class="fragment_input" style="width:100%;" rows="15" wrap="off"><?php if(isset($_GET["example"])){readfile("examples/pdb_stump2.pdb");} ?></textarea><br/>
	</div>
	<div style="text-align:center; margin-top:5%;">
	  Gap length:<br/>
	  -----<input type="number" name="gap" style="margin:5px;" size="4" min="1" max="50" step="1" value=<?php if(isset($_GET["example"])){echo "4"; }else{ echo "5";} ?> />-----
	</div>
	<div style="clear:both;">
	  Display <input type="text" id="displayNumber" name="displayNumber" size="3" value="70" /> results.
	  <input type="button" id="search_gapped" value="Search" onclick="submitForm(this.form)"/>
	</div>
      </form>
    </div>

  </div>
  <hr style="width:40%"/>
  <p style="text-align:center; font-size:0.8em;">
This server will store your search results for 30 days. If you wish to return to your results within 30 days either: enable cookies on this domain and click on the previous searches link above; or, bookmark the link in the search results window.
  </p>
  <div style="text-align:center;">
    <span style="text-align:center; border:2px solid red; display:inline-block; padding:5px;">
      Please email <a href="mailto:james.collier412@gmail.com">James Collier</a> with <b style="color:red;">bug reports</b> and feature requests.
    </span>
  </div>
  <p style="text-align:justify;">
This server searches for structural fragments of proteins using the <a href="http://gitorious.org/super/pages/Home/">Super</a> program on the <a href="http://www.pdb.org">PDB</a> database. For documentation on how to use the standalone version of this program see the documentation <a href="https://gitorious.org/super/pages/Using_Super">here</a>. The source code for this program is available online at <a href="http://gitorious.org/super">gitorious</a>.
  </p>
  <hr style="width:40%;" />
  <div style="margin:1em; padding:1em;">
    <p style="text-align:center">
      <h4><u>*NEWS*</u></h4>
      <ul>
	<?php $news = file("NEWS"); for($i=0; $i < count($news); $i++){ echo "<li>" . $news[$i]; } ?>
      </ul>
    </p>
  </div>
</body>
</html>
