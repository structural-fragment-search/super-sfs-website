<?php

/* START Setup Configuration */
require_once( "superSetup.php" );
/* END Setup Configuration */

function super_is_still_running(){
  $command = "ps aux | grep super | grep -vw grep";
  $running = shell_exec($command);
  return (strlen($running) != 0);
}


if(isset($_GET["from"])){
  $result = $_GET["from"] . "/results.1";
  $progress = $_GET["from"] . "/progress.1";
  $key = $_GET["key"];
} else {
  $result = "results.1";
  $progress = null;//"/dev/null";
  $key = "rmsd";
}

if(isset($_GET["number"])){
  $topN = (int)$_GET["number"];
} else {
  $topN = 20;
}

$completed = false;
if($progress){
  $get_last_line = "tail -1 " . $progress;
}else{
  $get_lest_line = "echo \"not tracking progress\"";
}

while ( super_is_still_running() ){
  echo "%" . shell_exec($get_last_line);
  exit;
}

chdir($_GET["from"]);
if(! file_exists("amalgamated")){
  shell_exec("../../../sortNresults.sh $topN $result");
  echo "%" . shell_exec($get_last_line);
} else {
  if(filesize("sorted." . $key) > 0){
    echo "!" . `cat sorted.rmsd | wc -l`;
    echo `head -$topN sorted.$key`;
  }else{
    echo "^Sorry, no matches were found.";
  }
}
?>
