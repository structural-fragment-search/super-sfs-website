var num_results = 0;
var current_identifier = 0;
var progress_fraction = 0;
var waiting = 0;
var waiting_delta = 0.1;
var query_sequence = null;
var rows = 0;
var file = null;

var sequenceColours = new Array();
sequenceColours["A"] = "style=\"color:red\"";
sequenceColours["B"] = "";
sequenceColours["C"] = "style=\"color:green\"";
sequenceColours["D"] = "style=\"color:blue\"";
sequenceColours["E"] = "style=\"color:blue\"";
sequenceColours["F"] = "style=\"color:red\"";
sequenceColours["G"] = "style=\"color:green\"";
sequenceColours["H"] = "style=\"color:green\"";
sequenceColours["I"] = "style=\"color:red\"";
sequenceColours["J"] = "";
sequenceColours["K"] = "style=\"color:magenta\"";
sequenceColours["L"] = "style=\"color:red\"";
sequenceColours["M"] = "style=\"color:red\"";
sequenceColours["N"] = "style=\"color:green\"";
sequenceColours["O"] = "";
sequenceColours["P"] = "style=\"color:red\"";
sequenceColours["Q"] = "style=\"color:green\"";
sequenceColours["R"] = "style=\"color:magenta\"";
sequenceColours["S"] = "style=\"color:green\"";
sequenceColours["T"] = "style=\"color:green\"";
sequenceColours["U"] = "";
sequenceColours["V"] = "style=\"color:red\"";
sequenceColours["W"] = "style=\"color:red\"";
sequenceColours["X"] = "";
sequenceColours["Y"] = "style=\"color:green\"";
sequenceColours["Z"] = "";


function resultObject(pdbid, description, rmsd, chain, start, end, sequence, identity, similarity){
    this.ID = pdbid; //string
    this.chars_per_line = Math.floor((0.4 * dojo.byId("container").clientWidth)/7.4);
    if(description){
	this.title = description.substring(0, 240);
	this.description = description;
    } else {
	this.title = "?";
	this.description = "?";
    }
    this.RMSD = rmsd.toFixed(3); //float
    this.chain = chain; //string
    if(start[start.length-1] == '_'){
	this.s_res = start.substring(0, start.length-1);
    }else this.s_res = start; //string
    if(end[end.length-1] == '_'){
	this.e_res = end.substring(0, end.length-1);
    }else this.e_res = end; //string

    this.sequence = sequence;
    this.identity = identity;
    this.similarity = similarity;
    this.url = "http://www.pdb.org/pdb/explore/explore.do?structureId="+pdbid;
    this.dom_node = null;
    this.identifier = current_identifier++;
}

function set_result_sequence(sequence){
    var result_sequence = "";
    var partial_sequence = "";
    for(var j=0; j < rows; j++){
	partial_sequence = sequence.substring(j*50, ((j+1)*50)-1);
	for(i=0; i < partial_sequence.length; i++){
	    result_sequence = result_sequence+"<span "+sequenceColours[partial_sequence.charAt(i)]+">"+partial_sequence.charAt(i)+"</span>";
	}
	dojo.byId("result-sequence"+j).innerHTML=result_sequence;
	result_sequence = "";
    }
}

function set_query_sequence(sequence){
    if(query_sequence == null){
	rows = (Math.ceil(sequence.length / 50) > 4 ? 4 : Math.ceil(sequence.length / 50));
	query_sequence = "";
	var partial_sequence = "";
	for(var j=0; j < rows; j++){
	    partial_sequence = sequence.substring(j*50, ((j+1)*50)-1);
	    for(i=0; i < partial_sequence.length; i++){
		query_sequence = query_sequence+"<span "+sequenceColours[partial_sequence.charAt(i)]+">"+partial_sequence.charAt(i)+"</span>";
	    }
	    dojo.style("s"+j, "visibility", "visible");
	    dojo.byId("query-sequence"+j).innerHTML=query_sequence;
	    query_sequence = "";
	}
    }
}

function display_result(result){
	var dom_node = {
	    id:result.identifier.toString(),
	    style: { marginBottom:"5px" },
	    innerHTML: "<span class=\"pdbid\"><a target=\"_blank\" href=\""+result.url+"\">"+result.ID+"</a></span><span id=\""+result.identifier+"-ops\" class=\"operations\"><a href=\"javascript:view_in_jmol("+result.identifier+");\">View</a></span><span class=\"identity\">"+result.identity+"%</span><span class=\"similarity\">"+result.similarity+"%</span><span class=\"rmsd\">"+result.RMSD+"</span><span class=\"match-data\">"+result.s_res+" &mdash; "+result.e_res+"</span><span class=\"chain\" style=\"width:10px;\">"+result.chain+"</span><div class=\"description\" title=\""+result.description+"\">"+result.title+"</div>"};

	if(Results.length == 1){
	    result.dom_node = dojo.create("div", dom_node, "columnTitles", "after");
	}else{
	    result.dom_node = dojo.create("div", dom_node, Results[Results.length-2].identifier.toString(), "after");
	}
}

function retrieve_results(from_file, number, key) {
    var xhrArgs = {
	url: "get_next_result.php",
	handleAs: "text",
	content: {
	    from: from_file,
	    number: number,
	    key: key
	},
	load: function(data){
	    if(data[0] == '!'){
		clearInterval(timer);
		number_of_results = data.substring(1, data.indexOf("\n"));
		set_number_of_results(parseInt(number_of_results));
		data = data.substring(data.indexOf("\n")+1);
		dojo.style("progress", "width", "100%");
		dojo.byId("pb_text").innerHTML = "Done";
		dojo.style("loadIndicator", "visibility", "hidden");
		display_download();
		dojo.removeAttr("title_update_button", "disabled");
	    }else if(data[0] == "%"){
		var p = parseInt(data.split("%")[1]);
		if(!isNaN(p)){
		    dojo.style("progress", "width", p+"%");
		    dojo.byId("pb_text").innerHTML = "Running Search: "+p+"%";
		}
		return;
	    }else if(data[0] == '^'){ //no results
		clearInterval(timer);
		dojo.style("progress", "width", "100%");
		dojo.byId("pb_text").innerHTML = "Done";
		dojo.style("loadIndicator", "visibility", "hidden");
		dojo.removeAttr("title_update_button", "disabled");
		var message = data.substring(1);
		dojo.byId("container").innerHTML = "<div style=\"backgroundColor:lightgray;\"><b>"+data.substring(1)+"</b></div>";
		return;
	    }

	    var tops = data.split("\n");
	    /* Webkit doesn't support the for each construct so it can't be used yet: for each (var result in tops){ */
	    for (var r = 0; r < tops.length; r++){
		if(Results.length == parseInt(number)){return;}
		if(tops[r].length == 0) continue;
		var parse_res = tops[r].split(" ");
		var residues = parse_res[2].split(":");
		var seq = parse_res[4].split(":");
		var identity = parse_res[5];
		var similarity = parse_res[6];
		var description = parse_res[7].replace(/_/g, " ");
		var res = new resultObject(parse_res[0], description,
					   parseFloat(parse_res[3]),
					   parse_res[1],
					   residues[0], residues[1],
					   seq[0],
					   identity, similarity);
		if(query_sequence == null){
		    set_query_sequence(seq[1]);
		}
		Results.push(res);
		display_result(res);
	    }
	    if(Results.length < dojo.byId("resultCountEditor").value){
		dojo.byId("resultCountEditor").value = Results.length;
	    }
	},
	error: function(error) {
	    if(error){
		dojo.byId("container").innerHTML = "An error occured: " + error;
	    }else{
		dojo.byId("container").innerHTML = "There are no results to display.";
	    }
	}
    };

    dojo.xhrGet(xhrArgs);
    return true;
}


//Viewer stuff for Jmol!!

/*
  Call PHP server-side with ajax to do transformations, then load stuff into Jmol
  Argument result: is a resultObject and contains all needed information
  Argument search_directory: used by the transform_result.php script to place resulting files
*/
function setup_jmol_view(result, search_directory){
    var xhrArgs = {
	url: "transform_result.php",
	handleAs: "text",
	content: {
	    from:search_directory,
	    pdbid:result.ID,
	    chain:result.chain,
	    start:result.s_res,
	    end:result.e_res,
	},
	load: function(data){
	    files = data.split("|");	    
	    load_jmol_view(files[0], files[1]);
	},
	error: function(error) {
	    alert("Returned fail!");
	}
    };

    dojo.xhrGet(xhrArgs);
    return true;
}


function update_search_title(sid, cnt){
    var title = dojo.byId("query-title").value;
    var xhrArgs = {
	url: "update_search_title.php",
	handleAs: "text",
	content: {
	    newtitle:title,
	    session:sid,
	    count:cnt
	},
	load: function(data){
	    dojo.attr("bookmark_link", "href", search_link+encodeURI(title));
	    dojo.byId("page_title").innerHTML=title;
	},
	error: function(error){
	    alert("Failed to change the search title: "+error);
	}
    };
    dojo.xhrPost(xhrArgs);
    return;
}