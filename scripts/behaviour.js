/*  The website software interface for the Super protein
    structural fragment search program.
    This file specifies functions defining the behaviour of the page.
    Copyright (C) 2011  James Collier

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

var lastCheckedPDBId = null;

function get_available_chains(id){
    var input = "pdb_id_id"+id;
    dojo.style(input, "backgroundColor", "red");
    var targetSelect = dojo.byId("pdb_id_chain"+id);
    target_fragments[parseInt(id)].valid = false;
    targetSelect.innerHTML = "";
    var val = dojo.byId(input).value;
    if(val.length == 4){
	//Check it!
	var xhrArgs = {
	    url: "get_available_chains.php",
	    handleAs: "text",
	    content: {
		pdbid:val,
	    },
	    load: function(data){
		if(data[0] != '!'){
		    dojo.style(input, "backgroundColor", "green");
		    var avChains = data.split("\n");
		    var targetSelect = dojo.byId("pdb_id_chain"+id);
		    targetSelect.innerHTML = "";
		    for(var i = 0; i < avChains.length-1; i++){
			targetSelect.options[i] = new Option(avChains[i], avChains[i]);
		    }
		    target_fragments[parseInt(id)].valid = true;
		}else{
		    var targetSelect = dojo.byId("pdb_id_chain"+id);
		    targetSelect.innerHTML = "";
		    target_fragments[parseInt(id)].valid = false;
		}
	    },
	    error: function(error) {
		alert("Returned fail from server for PDB ID check!");
		target_fragments[parseInt(id)].valid = false;
	    }
	};
	dojo.xhrGet(xhrArgs);
	return true;
    }
}

function specifyObject(id, parent){
    this.identifier = id+1;
    this.valid = false;
    this.pdbidName = "pdb_id_id"+this.identifier;
    this.chainName = "pdb_id_chain"+this.identifier;
    this.startName = "pdb_id_start"+this.identifier;
    this.endName = "pdb_id_end"+this.identifier;
    this.pdbid = dojo.create("input", {id:this.pdbidName,
				       name:this.pdbidName,
				       type:"text",
				       size:10,
				       onkeyup:"get_available_chains("+this.identifier+")"},
			     parent);
    this.chain = dojo.create("select", {id:this.chainName,
					name:this.chainName},
			     parent);
    this.start = dojo.create("input", {id:this.startName,
				       name:this.startName,
				       type:"text",
				       size:6},
			     parent);
    this.end = dojo.create("input", {id:this.endName,
				     name:this.endName,
				     type:"text",
				     size:6},
			   parent);
}

function create_new_specify_input(previous){
    var newid = previous.identifier + 1;
    var div = dojo.create("div", {id:"pdb_id_input"+newid}, "pdb_id_input"+previous.identifier, "after");
    var newform = new specifyObject(previous.identifier, div);
    target_fragments.push(newform);
}

function addFragment(button_id){
    var data;
    var new_data;
    var count;

    if(button_id == "add_frag"){
	count = dojo.byId("paste_count");
	count.value = 1 + parseInt("0" + count.value, 10);
	data = copied_fragments[copied_fragments.length-1];
	new_data = dojo.create("textarea", {name:"input" + parseInt(count.value), rows:"15", cols:"80", className:"fragment_input"}, data, "after");
	copied_fragments.push(new_data);
    }else{
	data = target_fragments[target_fragments.length-1];
	create_new_specify_input(data);
    }
}

function submitForm(form_id){
    document.forms[form_id.name].target="_blank";
    if(form_id.name == "PDB_paste_query"){
	var data = dojo.byId("fragment_input1");
	if(data.value.length == 0){
	    dojo.style("error1", "visibility", "visible");
	    return;
	}
    }else if(form_id.name == "PDB_specify_query"){
	for(var i = 0; i < target_fragments.length; i++){
	    if(target_fragments[i].valid == false){
		dojo.style("warn2", "visibility", "visible");
		return;
	    }
	}
	for(var i = 0; i < target_fragments.length; i++){
	    if(dojo.byId(target_fragments[i].pdbidName).value.length == 0){
		dojo.style("error2", "visibility", "visible");
		return;
	    }
	    if(dojo.byId(target_fragments[i].chainName).options[dojo.byId(target_fragments[i].chainName).selectedIndex].value.length == 0){
		dojo.style("error2", "visibility", "visible");
		return;
	    }
	    if(dojo.byId(target_fragments[i].startName).value.length == 0){
		dojo.style("error2", "visibility", "visible");
		return;
	    }
	    if(dojo.byId(target_fragments[i].endName).value.length == 0){
		dojo.style("error2", "visibility", "visible");
		return;
	    }
	}
    }
    document.forms[form_id.name].submit();
}

function clearErrors(id){
    dojo.style("error"+id, "visibility", "hidden");
    dojo.style("warn"+id, "visibility", "hidden");
}