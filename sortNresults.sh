#!/bin/sh

N=$1
FILE=$2
if [ ! -s $FILE ]; then
    exit;
fi

## AMALGAMATE ##
if [ -f ./amalgamated ]; then
    exit;
else
    touch ./amalgamated;
fi

for resfile in ./results.[0-9]; do
    echo "#Query ${COUNTER}" >> ./results.txt;
    cat $resfile | ../../../sequence_similarity.pl >> ./results.txt;
    let COUNTER=$COUNTER+1;
done
##### END #####

cat $FILE | perl ../../../sequence_similarity.pl | sort -k1 > id_sorted.tmp
join id_sorted.tmp ../../../descriptions.txt | sort -n -k4 > sorted.rmsd
sort -r -n -k6 sorted.rmsd > sorted.identity
sort -r -n -k7 sorted.rmsd > sorted.similarity
rm id_sorted.tmp
