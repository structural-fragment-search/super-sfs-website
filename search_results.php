<?php
error_reporting(E_ALL | E_STRICT);

/* START Setup Configuration */
require_once( "superSetup.php" );
/* END Setup Configuration */

session_name("_SuperUserID");
session_set_cookie_params(30 * 24 * 60 * 60, $super_web_path);
session_start();

if(isset($_SESSION)){
  if(isset($_SESSION["search_count"])){
    $search_count = & $_SESSION["search_count"];
    $search_count++;
  } else {
    $_SESSION["search_count"] = 1;
    $search_count = & $_SESSION["search_count"];
  }
}

$sessionID = session_id();
$search_title = "Session Broken!";

$session_dir = "$super_path/searches/" . $sessionID;
$file = stat($session_dir);
if(!$file){
   mkdir($session_dir);
}


if(isset($_POST["method"])){
  $method = $_POST["method"];
  $threshold = $_POST["threshold"];
  $numResults = $_POST["displayNumber"];
  $number_of_queries = $_POST["number"];
  $database = $_POST["database"];
}else if(isset($_GET["method"])){
  $_SESSION["search_count"]--;
  $method = $_GET["method"]; //this will be "retrieve"
  $search_count = $_GET["search_count"];
  $threshold = $_GET["threshold"];
  $numResults = $_GET["nresults"];
  $number_of_queries = $_GET["nqueries"];
  $search_title = urldecode($_GET["title"]);
  $database = urldecode($_GET["database"]);
}

$search_dir = "$session_dir/search${search_count}";
$relative_search_dir = "searches/$sessionID/search${search_count}/";
$file = stat($search_dir);
if(!$file){
   mkdir($search_dir);
} else if ($method != "retrieve"){
  /* Remove contents*/
  $d = dir($search_dir);
  while($entry = $d->read()){
    if($entry != "." && $entry != ".."){
      unlink("$search_dir/$entry");
    }
  }
  $d->close();

  /* Update history,
     repopulate */
  $saved_searches = "$session_dir/saved_searches.html";
  $exclude = "id=$search_count "; //Need the space afterward to distinguish "4", "40", "45", ...
  $list_saved_searches = file($saved_searches);
  $new_saved_searches = "";
  $out = "";
  foreach($list_saved_searches as $line){
    if(strstr($line, $exclude) == ""){
      $out .= $line;
    }
  }
  $d = fopen($saved_searches, "w");
  fwrite($d, $out);
  fclose($d);
}

$databasename = "Non-redundant PDB";
if($database == "pdb.db"){
  $databasename = "PDB";
}

if ($method == "paste")
{
   $search_title = "Pasted query $search_count";
   $current = 1;
   while ($current <= $number_of_queries) { //>{
     $qry = "$search_dir/query${current}.pdb";
     $qry_db = "$search_dir/query${current}.db";
     $fd = fopen($qry, "wb");
     fwrite($fd, $_POST["input$current"]);
     fclose($fd);
		      
     $parser_output = shell_exec("$Python_Binary pdb_pp.py --query $qry --output $qry_db");
     $current++;
   }

   /* Run the search */
   $current = 1;
   while ($current <= $number_of_queries) {
     $qry_db = "$search_dir/query${current}.db";
     $search_output = shell_exec("LD_LIBRARY_PATH=$super_plugin_path $super_path/super -l arithmetic -c $CPUCores -t $threshold -q $qry_db $database > $search_dir/results.${current} 2> $search_dir/progress.${current} &");
     $current++;
   }
   shell_exec("date | sed s/^/#/ > $search_dir/results.txt");
   shell_exec("echo \"# Title: $search_title \" >> $search_dir/results.txt");
   shell_exec("echo \"# Threshold: $threshold \" >> $search_dir/results.txt");
}else if($method == "specify"){
  $search_title = "Specified query $search_count";
  $current = 1;
  $cid = 0;
  while ($current <= $number_of_queries) {
    preg_match('/([\d]+[a-zA-Z]*)/', $_POST["pdb_id_start$cid"], $startext); /* Does this keep an insertion code? */
    preg_match('/([\d]+[a-zA-Z]*)/', $_POST["pdb_id_end$cid"], $endext); /* Does this keep an insertion code? */
    $qry = "$search_dir/query${current}.pdb";
    $qry_db = "$search_dir/query${current}.db";
    $id = strtolower($_POST["pdb_id_id$cid"]);
    $directory = substr($id, 1, 2);
    $chain = $_POST["pdb_id_chain$cid"];
    $start = $startext[1];
    $end = $endext[1];
    $command = "$Python_Binary get_coords.py -g -c $chain -f $start -t $end $pdb_path/$directory/pdb${id}.ent.gz $qry";
    shell_exec($command);
		      
    $parser_output = shell_exec("$Python_Binary pdb_pp.py --query $qry --output $qry_db");
    $current++;
    $cid++;
  }

  /* Run the search */
  $current = 1;
  while ($current <= $number_of_queries) {
    $qry_db = "$search_dir/query${current}.db";
    shell_exec("LD_LIBRARY_PATH=$super_plugin_path $super_path/super -l arithmetic -c $CPUCores -t $threshold -q $qry_db $database > $search_dir/results.${current} 2> $search_dir/progress.${current} &");
    $current++;
  }
  shell_exec("date | sed s/^/#/ > $search_dir/results.txt");
  shell_exec("echo \"# Title: $search_title \" >> $search_dir/results.txt");
  shell_exec("echo \"# Threshold: $threshold \" >> $search_dir/results.txt");
}else if($method == "stumps"){
  $search_title = "Gapped query $search_count";
  $current = 1;

  $stump1 = "$search_dir/stump1.pdb";
  $stump2 = "$search_dir/stump2.pdb";
  $qry_db = "$search_dir/query1.db";
  $fd = fopen($stump1, "wb");
  fwrite($fd, $_POST["stump1"]);
  fclose($fd);
  $fd = fopen($stump2, "wb");
  fwrite($fd, $_POST["stump2"]);
  fclose($fd);
  shell_exec("cat $stump1 $stump2 > $search_dir/query1.pdb");
  shell_exec("echo " . $_POST["gap"] . " > $search_dir/gap_data");
  shell_exec("$Python_Binary countCAs.py $search_dir/stump1.pdb > $search_dir/stump_data");
  $parser_output = shell_exec("$Python_Binary pdb_pp.py --query $stump1 $stump2 --output $qry_db");
  
  /* Run the search */
  $search_output = shell_exec("LD_LIBRARY_PATH=$super_plugin_path $super_path/super -l none -c $CPUCores -t $threshold --random-coil-length=" . $_POST["gap"] . " -q $qry_db $database > $search_dir/results.1 2> $search_dir/progress.${current} &");

  shell_exec("date | sed s/^/#/ > $search_dir/results.txt");
  shell_exec("echo \"# Title: $search_title \" >> $search_dir/results.txt");
  shell_exec("echo \"# Threshold: $threshold \" >> $search_dir/results.txt");
}else if($method == "upload"){
  $qry_tar = $search_dir . "/queries.tar";
  $search_title = "Uploaded";
  if(($_FILES["fragments"]["type"] == "application/x-tar") 
     && ($_FILES["fragments"]["size"] < 1000000)){
    if($_FILES["fragments"]["error"] > 0){
      $search_title = $search_title . "Return Code: " . $_FILES["fragments"]["error"] . "<br/>";
    }else{
      if(!file_exists($qry_tar)){
	move_uploaded_file($_FILES["fragments"]["tmp_name"], $qry_tar);
	$command = "tar -tf " . $qry_tar;
	$taroutput = shell_exec($command);
	$files = explode("\n", $taroutput);
	$command = "tar -xf " . $qry_tar . " -C " . $search_dir;
	shell_exec($command);
      }else{
	$search_title = "Apparently " . $qry_tar . " already exists!";
      }
    }
  }else{
    $search_title = "Problem";
    exit;
  }
  for($i = 0; $i < sizeof($files); ++$i){
    $qry = $search_dir . '/' . $files[$i];
    $qry_db = $search_dir . '/' . $files[$i] . '.db';
    shell_exec("$Python_Binary pdb_pp.py --query  $qry --output $qry_db");
    shell_exec("LD_LIBRARY_PATH=$super_plugin_path $super_path/super -l arithmetic -c $CPUCores -t $threshold -q $qry_db $database > $search_dir/results." . $i+1 . " 2> $search_dir/progress." . $i+1 . " &");
  }
}else if($method == "retrieve"){
}

$link = "search_results.php?method=retrieve&search_count=" . $search_count . "&threshold=" . $threshold . "&nresults=" . $numResults . "&nqueries=" . $number_of_queries . "&database=" . $database . "&title=" . rawurlencode($search_title);

if($method != "retrieve"){
  $save_search = date("d-m-Y H:i:s") . " Using:" . $databasename . " <a id=" . $search_count . " href=\\\"" . $link . "\\\">" . $search_title . " @ T: " . $threshold . "&Aring;</a><br/>";
  $save_search_command = "echo \"" . $save_search . "\" >> " . $session_dir . "/saved_searches.html";
  shell_exec($save_search_command);
}
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title id="page_title"><?php if($method == "retrieve"){echo $search_title . " @ " . $threshold . "&Aring;";}else{ echo "Super: Search Results $search_count";} ?></title>
    <link rel="stylesheet" type="text/css" href="styles/super.css" />
    <script src="jmol/Jmol.js"></script>
    <script src="scripts/dojo.js"></script>
    <script src="scripts/mootools.js"></script>
    <script src="scripts/ScrollSpy.js"></script>

    <!-- This code sorts and displays incoming results -->
    <script>
      var Results = new Array(); //Storage for displayed results
      var sort_key = "rmsd";
      var timer = null;
      var jmolExpandedViewer = null;

      var thedatabase = "<?php echo $database; ?>";
      var search_link = "<?php echo "search_results.php?method=retrieve&search_count=$search_count&threshold=$threshold&nresults=$numResults&nqueries=$number_of_queries&database=$database&title="; ?>";

      function display_download(){
	  if(dojo.query("#downloads").length){
	  }else{
              dojo.create("p", {id:"downloads", style: {fontSize:"0.8em", textAlign:"right"}, innerHTML: "Download complete results as: <a target=\"_blank\" href=\"<?php echo $relative_search_dir . "/results.txt" ?>\">text file</a>."}, "container", "first");
	  }
      }
    </script>

    <script src="scripts/retrieve_results.js"></script>
    <script>
      var expanded = false;

      dojo.ready(function(){
        var demoTwo = new ScrollSpy({
          min: 0, // acts as position-x: absolute; left: 20px;
          mode: 'horizontal',
          onEnter: function(position,enters) {
      
          },
          onLeave: function(position,leaves) {
      
          },
            onTick: function(position,state,enters,leaves) {
              $("display").style.left = -position.x+19+"px";
          },
            container: window
        });
	  dojo.style("sort_rmsd", "color", "Green");
	  /* Start getting results through Ajax :) */
	  retrieve_results(<?php echo "\"$search_dir/\""; ?>, <?php echo "\"$numResults\""; ?>, sort_key);
	  timer = setInterval("retrieve_results(\"<?php echo $search_dir . "/"; ?>\", \"<?php echo $numResults; ?>\", sort_key)", 1000);
      });

      function show_div(d){
        dojo.style(d, "display", "block");
      }

      function hide_div(d){
        dojo.style(d, "display", "none");
      }

      var previous_selection = null;

      function view_in_jmol(id){
	  hide_div("dl_qry_t");
          set_result_sequence(Results[id].sequence);
          var res = null;
          dojo.style(id.toString()+"-ops", "backgroundColor", "red");
	  if(previous_selection){
              dojo.style(previous_selection, "backgroundColor", "LightCyan");
	  }
          previous_selection = id.toString()+"-ops";
	  jmolScript("set echo top left; font echo 15 sansserif plain; color echo yellow; echo \"Loading structure from wwPDB...\";");
	  dojo.style("loadIndicator", "visibility", "visible");
	  dojo.byId("pb_text").innerHTML = "Loading structure from wwPDB...";
          setup_jmol_view(Results[parseInt(id)], <?php echo "\"$search_dir/\""; ?>);
      }

      function load_jmol_view(pdbfile, qryfile){
	  jmolScript("load "+pdbfile+"; color blue; load append "+qryfile+"; select 2.1; color red; frame *; hide not backbone; select backbone; spacefill off; backbone 0.3");
	  dojo.style("loadIndicator", "visibility", "hidden");
	  dojo.byId("pb_text").innerHTML = "Done";
	  show_div("dl_qry_t");
      }

      function jmol_toggle_expanded_view(){
        if(expanded){
          dojo.style("display", "width", "300px");
          dojo.style("display", "height", "300px");
          dojo.style("display", "zIndex", "800000");
          dojo.style("display", "display", "inline");
          dojo.style("display", "position", "fixed");
          jmolResizeApplet(300);
          expanded = false;
          dojo.byId("expander").innerHTML = "Expand";
        }else{
          dojo.style("display", "width", "800px");
          dojo.style("display", "height", "900px");
          dojo.style("display", "zIndex", "800000");
          dojo.style("display", "display", "block");
          dojo.style("display", "position", "absolute");
          jmolResizeApplet(800);
          expanded = true;
          dojo.byId("expander").innerHTML = "Collapse";
        }
      }

      function get_more_results(){
	  current_identifier = 0;
	  dojo.byId("pb_text").innerHTML = "Fetching your results...";
	  dojo.style("loadIndicator", "visibility", "visible");
	  retrieve_results(<?php echo "\"$search_dir/\""; ?>, dojo.byId("resultCountEditor").value, sort_key);
          while ( Results.length > 0 ) {
	      var drop = Results.pop();
	      dojo.destroy(drop.dom_node);
	  }
      }

      function sort_on_rmsd(){
          sort_key = "rmsd";
	  dojo.style("sort_rmsd", "color", "Green");
	  dojo.style("sort_identity", "color", "Blue");
	  dojo.style("sort_similarity", "color", "Blue");
	  get_more_results()
      }
      
      function sort_on_identity(){
          sort_key = "identity";
	  dojo.style("sort_rmsd", "color", "Blue");
	  dojo.style("sort_identity", "color", "Green");
	  dojo.style("sort_similarity", "color", "Blue");
	  get_more_results();
      }
      
      function sort_on_similarity(){
          sort_key = "similarity";
	  dojo.style("sort_rmsd", "color", "Blue");
	  dojo.style("sort_identity", "color", "Blue");
	  dojo.style("sort_similarity", "color", "Green");
	  get_more_results();
      }

      function update_search_title_inline(){
	  update_search_title("<?php echo $sessionID; ?>", "<?php echo $search_count; ?>");
      }

      function set_number_of_results(number){
	  dojo.byId("total_results").innerHTML = number.toString();
	  dojo.attr("resultCountEditor", "max", number.toString());
      }
    </script>
<?php readfile("scripts/analytics.js"); ?>
  </head>
  <body>
    <noscript>
      Note that this site requires client side processing using javascript. Please enable javascript in your web-browser and/or disable any javascript blocking plugins you have be using.
    </noscript>
    <p id="citation"><span>Citation:</span> <a href="http://lcb.infotech.monash.edu.au/wiki/index.php/User:James">James H. Collier</a>, <a href="http://bmb.psu.edu/directory/aml25">Arthur M. Lesk</a>, <a href="http://www.csse.monash.edu.au/~mbanda/">Maria Garcia de la Banda</a> and <a href="http://www.csse.monash.edu.au/~karun/Site/Home.html">Arun S. Konagurthu</a>, Super: A web server to rapidly screen superposable oligopeptide fragments from the Protein Data Bank. <i>Nucleic Acids Research</i> (in press; to appear in July 2012 Web Server Issue)</p>
    <div id="main">
      <div id="header">
	<div id="session">
	<b>Title:</b><input id="query-title" type="text" size="20" value="<?php echo $search_title; ?>"/>@<?php echo $threshold . "&Aring;."; ?><input type="button" value="update" id="title_update_button" onclick="update_search_title_inline();" disabled="disabled"/><br/>
	  <p style="font-size:0.8em; padding:2px; margin:0; background-color:LightGray; width:80%">UserID: <?php echo $sessionID ?></p>
	</div>
	<div id="status">
	  <!-- Start ProgressBar ...-->
	  <div class="progressbar">
	    <div id="progress" class="pb_value" style="background-color:#00aa00; width:0%;">
	      <div id="pb_text" class="pb_text">
		Running Search: 0%
	      </div>
	    </div>
	    <span id="loadIndicator" style="position:absolute; top:5px; right:5px; visibility:visible;">
	      <img src="images/ajax-loader.gif"/>
	    </span>
	  </div>
	  <!-- End ProgressBar -->
	  <!-- Firefox doesn't yet support HTML5 input type "number", so use "onchange" to notify the server of a request -->
	  <span id="resultCount">Showing <input id="resultCountEditor" type="text" pattern="[0-9]*" size="3" min="10" max="100" step="10" value="<?php echo $numResults; ?>" onchange="get_more_results()"/> of <span id="total_results">?</span> results from <?php echo $databasename; ?>.</span><span style="float:right;">Return here in the future: <a id="bookmark_link" href="<?php echo $link; ?>">bookmark this link</a>.</span>
	</div>
	<div style="clear:both;"></div>
      </div>
      <div class="body">
	<div id="display">
	  <script>
	    jmolInitialize("jmol/");
	    jmolSetLogLevel(5);
	    jmolApplet(300, "load \"<?php echo "$relative_search_dir/query1.pdb"; ?>\"; spacefill off; hide ! backbone; select backbone; wireframe off; color red; backbone 0.3;");
	  </script>
	  <span><a id="expander" href="javascript:jmol_toggle_expanded_view()">Expand</a></span>
	  <div id="dl_qry_t" style="float:right; display:none;"><a href="<?php echo "searches/" . $sessionID . "/search" . $search_count . "/qry-t.pdb";  ?>" title="Download transformed query">Transformed Query</a></div>
	  <br/><br/>
	  <p id="sequences-title">Sequence Comparison</p>
	  <div id="s0" style="visibility:visible;">
	    <div class="sequence-label" style="background:red;">Query&nbsp;<div class="sequence-display" id="query-sequence0"></div></div>
	    <br/>
	    <div class="sequence-label" style="background:blue; color:white;">Result<div class="sequence-display" id="result-sequence0"></div></div>
	  </div>
	  <div id="s1" style="visibility:hidden;">
	    <hr />
	    <div class="sequence-label" style="background:red;">Query&nbsp;<div class="sequence-display" id="query-sequence1"></div></div>
	    <br/>
	    <div class="sequence-label" style="background:blue; color:white;">Result<div class="sequence-display" id="result-sequence1"></div></div>
	  </div>
	  <div id="s2" style="visibility:hidden;">
	    <hr />
	    <div class="sequence-label" style="background:red;">Query&nbsp;<div class="sequence-display" id="query-sequence2"></div></div>
	    <br/>
	    <div class="sequence-label" style="background:blue; color:white;">Result<div class="sequence-display" id="result-sequence2"></div></div>
	  </div>
	  <div id="s3" style="visibility:hidden;">
	    <hr/>
	    <div class="sequence-label" style="background:red;">Query&nbsp;<div class="sequence-display" id="query-sequence3"></div></div>
	    <br/>
	    <div class="sequence-label" style="background:blue; color:white;">Result<div class="sequence-display" id="result-sequence3"></div></div>
	  </div>
	</div>
	<div id="container" class="container">
	  <div id="results" style="margin-left:330px;"><!--This div contains the results data and expands with the frame size-->
	    <div id="columnTitles" >
	      <span class="columnHeader" style="float:left;"><u>PDBID</u></span>
	      <span class="columnHeader" style="float:right; padding-right:40px;" title="Sort by sequence identity">
		<a id="sort_identity" href="javascript:sort_on_identity();">%IDNTY</a>
	      </span>
	      <span class="columnHeader" style="float:right;" title="Sort by sequence similarity">
		<a id="sort_similarity" href="javascript:sort_on_similarity();">%SMLRTY</a>
	      </span>
	      <span class="columnHeader" style="float:right;" title="Sort by RMSD">
		<a id="sort_rmsd" href="javascript:sort_on_rmsd();">RMSD</a>
	      </span>
	      <span class="columnHeader" style="float:right;"><u>RESIDUES</u></span>
	      <span class="columnHeader" style="float:right;"><u>CHAIN</u></span>
	      <div class="description" style="background-color:LightGrey; font-size:0.8em;"><u>STRUCTURE TITLE</u></div>
	    </div>

	  </div>
	</div>
	<!-- Add link using PHP to further results. -->
	<div style="clear:both;"></div>
      </div>
    </div>
  </body>
</html>
